<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Page</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <?php include('css.php')?>
</head>
<body>

    <div class="container mt-6"><br>
    <h5>ลงทะเบียนผู้สมัคร</h5><hr>
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <?php if(isset($validation)): ?>
                    <div class="alert alert-danger"><?= $validation->listErrors(); ?></div>
                <?php endif; ?>
                <form action="/register/save" method="post">
                    <div class="mb-3">
                        <label for="inputname" class="form-label">ชื่อจริง</label>
                        <input type="text" name="Fname" class="form-control" id="inputforname" value="<?= set_value('Fname'); ?>">
                <div class="mb-3">
                        <label for="inputname" class="form-label">นามสกุล</label>
                        <input type="text" name="Lname" class="form-control" id="inputforname" value="<?= set_value('Lname'); ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputname" class="form-label">เลขบัตรประชาชน</label>
                        <input type="text" name="IDcard" class="form-control" id="inputforname" value="<?= set_value('IDcard'); ?>">
                    </div>
                 
                        <label for="inputemail" class="form-label">Email</label>
                        <input type="email" name="Email" class="form-control" id="inputforemail" value="<?= set_value('Email'); ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputpassword" class="form-label">รหัสผ่าน</label>
                        <input type="password" name="Password" class="form-control" id="inputforpassword">
                    </div>
                    <div class="mb-3">
                        <label for="inputconfpassword" class="form-label">ยืนยันรหัสผ่าน</label>
                        <input type="password" name="confpassword" class="form-control" id="inputforconfpassword">
                    </div>
                 <center><button type="submit" class="btn btn-warning">ลงทะเบียน</button>
                </form>
               
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>