<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
   

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style>
body { 
 background-color:			#F5F5DC;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.header {
  overflow: hidden;
  background-color: #CC9933;
  padding:10px 10px;
}
.red-box {
  		background:#000	;
          padding:8px 118px;
  	}
  p{
      background:  #CC9933	;
          padding:8px 8px;
          color: #FFFFFF;
          border: 1px solid white;
          font: 15px sans-serif;
     
    }
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 230px;
    height: 800px;
    background-color: #CC9933;
    font: 15px sans-serif;
    
}

li a {
    display: block;
    color: white;
    padding: 25px   30px 30px;
    text-decoration: none;
}
li a:hover {
    background-color:#CC9933;
    color: white;
}
ul {
    border: 1px solid white;
}
li {
   
    border-bottom: 1px solid white;
}
.container{
  
  position: absolute;
  top: 4em;
  padding-left: 200em;
  color:white;
  background :#CC9933;
  padding: 3px 10px 9px 10px;
  height: 10; width:470px;
  margin: 100px 20px 60px 690px;
}


</style>
</head>
<body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext"  ></div>
   <div class="red-box"></div>
    
    <div class="box">
      
<ul>
<li><a href="register.php" title="Register">กลับ</a></li>

  
<li><a href="register.php" title="Register">ลงทะเบียนเข้าใช้ระบบ</a></li>
<li><a href="" title="Login">เข้าสู่ระบบ</a></li>
&nbsp;
<li><a href="" title="Quota List" >สาขาและจำนวนที่รับสมัคร</a></li>&nbsp;
<li><a href="">ตอบคำถาม</a></li><lb>&nbsp;</lb><li><a href="" target="_blank">รายงานการรับสมัคร</a></li>
<li><a href="" target="_blank">ขั้นตอนการสมัคร</a></li>
</ul>
</div>
</td>
</head>
<body>

    <div class="container md-4 "><br>
 
            <h4 align="center">
            <span class="login"> </span>
           เข้าสู่ระบบ </h4><br>
           <hr>
                <?php if(session()->getFlashdata('msg')): ?>
                    <div class="alert alert-danger"><?= session()->getFlashdata('msg'); ?></div>
                <?php endif; ?>
                <form action="/login/auth" method="post">
                    <div class="mb-3">
            <form>
             <div class="form-group">
               <input name="" class="form-control" placeholder="Username" type="text">
                  </div><br>
                  <div class="form-group">
              <input name="" class="form-control" placeholder="Password" type="password">
                  </div> <br>
             <div class="form-group">
                   <label class="float-left custom-control custom-checkbox">
                   <a href="#" class="link-light">ยังไม่ได้ลงทะเบียน</a>  &ensp;   &ensp;  &ensp;&ensp; &ensp; &ensp; &ensp; &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                   <a href="#" class="link-light">ลืมรหัสผ่าน</a> </label></div><br>

                   <center> <button  class="btn  btn-warning btn-block ">Login</button></center><br><br>
                  </div> <br>
                
             <br>
            
        </form>
                
               
            </div>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>