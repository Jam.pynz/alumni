<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Document</title>
<style>
body { 
 background-color: 	#F5F5DC;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.header {
  overflow: hidden;
  background-color: #CC9933;
  padding:10px 10px;
}
.red-box {
  		background:#FFE4C4;
          padding:8px 118px;
  	}
.d-box {
  		background:#FFE4C4;
          padding:8px 118px;
  	}
.button{
  padding: 8px;
}

</style>
</head>
<body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext"  ></div>
   <div class="red-box"></div>
   

</div>

<div class="container">

<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">

    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
            aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
            aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
            aria-label="Slide 3"></button>
    </div>

    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="https://www.npru.ac.th/admin/file_images/20221005102754_01476efde7b5988372ebee90b6f08170.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://scontent.fkdt1-1.fna.fbcdn.net/v/t39.30808-6/289688613_5236935249724193_8689913868972604844_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeFL2-8BDTvd9k4YTgAgPSxEruPM0Eb4Lkyu48zQRvguTBREflOxf2oUu_T6isiL_KiyZNvvGJt9VBq6rcgS-Dm4&_nc_ohc=zrDYoicCAkMAX_3wTvo&_nc_ht=scontent.fkdt1-1.fna&oh=00_AfDUUNeUgtg4UCmDyo20sPaVAjo159cZynfcHlLEhEb1pw&oe=63621F24" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://www.npru.ac.th/admin/file_images/20221005102754_01476efde7b5988372ebee90b6f08170.jpg" class="d-block w-100" alt="...">
        </div>

    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
        data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>

    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
        data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>

 
 <div class="d-box"><br>
 <div class="d-grid gap-3 d-md-flex justify-content-md-center">
<button type="button" class="btn btn-outline-warning btn-lg">ลงทะเบียน</button>
<button type="button" class="btn btn-outline-warning btn-lg">เข้าสู่ระบบ</button>
<button type="button" class="btn btn-outline-warning btn-lg">ดูหลักสูตร</button>
<button type="button" class="btn btn-outline-warning btn-lg">ข่าวสาร<a href="single.html"></a></button>


</div>
</div><br>

<br><br><br><br><br><br><br><br><br>
<hr>


        <footer class="footer">
    <div class="container">
      <div class="row">
		
<div class="col-sm-6 col-md-6 col-lg-5">
          <div class="footer-logo">

            <div  class="col-md-12" style="font-size:18px;font-weight: 500;letter-spacing: 0.1em;" >มหาวิทยาลัยราชภัฏนครปฐม</div>
            <div  class="col-md-12" >Nakhon Pathom Rajabhat University </div>
			<div  class="col-md-12" ><i class="fa fa-map-marker-alt"></i> 85 ถนนมาลัยแมน อ.เมือง จ.นครปฐม 73000</div>
			<div  class="col-md-12" ><i class="fas fa-fax"></i> 0 3426 1048</div>
            <div  class="col-md-12" ><i class="fas fa-phone"></i> 0 3410 9300</div>
            <div  class="col-md-12" ><a href="mailto: saraban@npru.ac.th" target="_blank" style="color: #FFF; text-decoration:none;"><i class="fas fa-envelope"></i> saraban@npru.ac.th</a></div>
			  <div  class="col-md-12" ><a href="https://vt.tiktok.com/ZSR8UasYw/" target="_blank" style="color: #FFF; text-decoration:none;"><i class="fa-brands fa-tiktok"></i> pr_npru </a></div>
             <div class="col-md-12">
            <a href="https://www.facebook.com/NPRUPRUNIT" class="facebook" target="_blank" style="color: #FFF; text-decoration:none;"><i class="fab fa-facebook"></i> ประชาสัมพันธ์ ม.ราชภัฏนครปฐม PR NPRU</a><br/><br/>
            <!-- <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
            <a href="#" class="instagram"><i class="fab fa-instagram"></i></a> -->
            
          </div>
          </div>
		  <div class="col-md-12">
			  <iframe iframe width="100%" height="150"  src="https://maps.google.com/maps?width=100%&amp;height=450&amp;hl=en&amp;q=%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%A5%E0%B8%B1%E0%B8%A2%E0%B8%A3%E0%B8%B2%E0%B8%8A%E0%B8%A0%E0%B8%B1%E0%B8%8F%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B8%9B%E0%B8%90%E0%B8%A1+(Title)&amp;ie=UTF8&amp;t=&amp;z=15&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
	</div>
          
<p>
        </div>

        

       

        <div class="col-sm-6 col-md-6 col-lg-4">
          <div class="list-menu">
			<div  class="col-md-12" >
            <h4 style="color:#FFF;">ระบบสารสนเทศ</h4>

            <ul class="list-unstyled">
              <li><a href="http://reg2.npru.ac.th/registrar/apphome.asp" target="_blank">>> ระบบรับสมัครนักศึกษา</a></li>
              <li><a href="app1.php" target="_blank">>> ระบบสารสนเทศสำหรับนักศึกษา</a></li>
              <li><a href="app2.php" target="_blank">>> ระบบสารสนเทศสำหรับบุคลากร</a></li>
              <li><a href="app3.php" target="_blank">>> ระบบสารสนเทศสำหรับบุคคลทั่วไป</a></li>
            </ul>
            
            <h4 style="color:#FFF;">เว็บไซต์ภายในมหาวิทยาลัย</h4>

            <ul class="list-unstyled">
              <li><a href="http://www.npru.ac.th/npru_sitemap.php" target="_blank">>> ลิงค์เว็บไซต์หน่วยงานทั้งหมด</a></li>
              <li><a href="http://dept.npru.ac.th/cp/" target="_blank">>> เว็บไซต์อาจารย์</a></li>
              <li><a href="http://dept.npru.ac.th/cp/index.php?act=6a992d5529f459a44fee58c733255e86&lntype=extmod&sys=sys_article&dat=index&mac_id=2496" target="_blank">>> เว็บไซต์สาขาวิชา</a></li>
              <li><a href="http://phone.npru.ac.th/" target="_blank">>> เบอร์โทรศัพท์หน่วยงานภายใน</a></li>
            </ul>

			</div>
			  <div  class="col-md-12" >
       <!--  <script>
  (function() {
    var cx = '018387909514839141753:fztttpjzksc';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>  >--></div>
          </div>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-2">
          <div class="list-menu">

          <!-- IPv6-test.com button BEGIN -->
<div style="margin-top:10px; margin-bottom:10px;"><a href='http://ipv6-test.com/validate.php?url=www.npru.ac.th'><img src='https://www.npru.ac.th/2019/img/icon/button-ipv6-small.png' alt='ipv6 ready' title='ipv6 ready' border='0' /></a></div> 
<!-- IPv6-test.com button END -->

 <div id=ipv6_enabled_www_test_logo></div>
<script language="JavaScript" type="text/javascript">
    	var Ipv6_Js_Server ="https://";
	document.write(unescape("%3Cscript src='" + Ipv6_Js_Server + "www.ipv6forum.com/ipv6_enabled/sa/SA1.php?id=5676' type='text/javascript'%3E%3C/script%3E"));
</script>   
<a href='http://ipv6-test.com'><img src='http://v4v6.ipv6-test.com/imgtest.png' alt='ipv6 test' title='ipv6 test' border='0' /></a>
          </div>
        </div>

      </div>
    </div>

    <div class="copyrights">
      <div class="container">
        <p>Copyrights &copy; 2019 Nakhon Pathom Rajabhat University. All rights reserved</p>
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
          
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>-->
        </div>        
      </div>
    </div>

  </footer>


       