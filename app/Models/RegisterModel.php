<?php namespace App\Models;

use CodeIgniter\Model;

class RegisterModel extends Model {
    protected $table = 'login';
    protected $allowedFields = ['Fname', 'Lname','IDcard','Email', 'password'];
}