<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\RegisterModel;

class Register extends Controller {
    public function index() {
        // include helper form
        helper(['form']);
        $data = [];
        echo view('register', $data);
    }

    public function save() {
        // include helper form
        helper(['form']);
        // set rules validation form
        $rules = [
            'Fname' => 'required|min_length[3]|max_length[20]',
            'Lname' => 'required|min_length[3]|max_length[20]',
            'IDcard' => 'required|min_length[13]|max_length[13]',
            'Email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[login.email]',
            'Password' => 'required|min_length[6]|max_length[200]',
            'confpassword' => 'matches[Password]',
        ];
        if ($this->validate($rules)) {
            $model = new  RegisterModel();
            $data = [
                'Fname' => $this->request->getVar('Fname'),
                'Lname' => $this->request->getVar('Lname'),
                'IDcard' => $this->request->getVar('IDcard'),
                'Email' => $this->request->getVar('Email'),
                'Password' => password_hash($this->request->getVar('Password'), PASSWORD_DEFAULT),
            ];
            $model->save($data);
            return redirect()->to('/login');
        } else {
            $data['validation'] = $this->validator;
            echo view('register', $data);
        }
    }
}