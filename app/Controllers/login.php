<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\RegisterModel;

class Login extends Controller {
    public function index() {
        // include helper form
        helper(['form']);
        echo view('login');
    }

    public function auth() {
        $session = session();
        $model = new RegisterModel();
        $email = $this->request->getVar('Email');
        $password = $this->request->getVar('Password');
        $data = $model->where('Email', $email)->first();
        if ($data) {
            $pass = $data['Password'];
            $verify_password = password_verify($password, $pass);
            if ($verify_password) {
                $ses_data = [
                    'Fname' => $data['Fname'],
                    'Lname' => $data['Lname'],
                    'IDcard' => $data['IDcard'],
                    'Email' => $data['Email'],
                    'logged_in' => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/dashboard');
            } else {
                $session->setFlashdata('msg', 'Wrong password');
                return redirect()->to('/login');
            }
        } else {
            $session->setFlashdata('msg', 'Email not found');
            return redirect()->to('/login');
        }
    }

    public function logout() {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }
}